<!-- Игровой стол -->

	<div class="gamers-12 demo"><!-- Кол-во игроков за столом - gamers-2, gamers-3 ... gamers-12 --> <!-- Добавлять класс demo для демо-стола или main для основной игры -->
		<div class="container">
			<div class="col-md-12">
				<div class="game-zone">
					<img class="stretches" src="assets/img/table.png">
					<!-- Игроки -->
					<div class="gamer-item gamer-num-0">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 0...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-1">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 1...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-2">
						<div class="gamer-info"><p><a href="#" class="game-user admin">Admin 2...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>						
						<div class="video">
							<object type="application/x-shockwave-flash" data="assets/img/88.swf">	
							<param name="movie" value="assets/img/88.swf">	
							<param name="wmode" value="opaque">	
							<param name="flashvars" value="direct_link=true">	
							<embed src="assets/img/88.swf" wmode="opaque" flashvars="direct_link=true"></object>
						</div>
					</div>
					<div class="gamer-item gamer-num-3">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 3...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-4">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 4...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-5">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 5...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-6">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 6...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
						<div class="video">
							<object type="application/x-shockwave-flash" data="assets/img/88.swf">	
							<param name="movie" value="assets/img/88.swf">	
							<param name="wmode" value="opaque">	
							<param name="flashvars" value="direct_link=true">	
							<embed src="assets/img/88.swf" wmode="opaque" flashvars="direct_link=true"></object>
						</div>
					</div>
					<div class="gamer-item gamer-num-7">
						<div class="gamer-info"><p><a href="#" class="game-user admin">Admin 7...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-8">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 8...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-9">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 9...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-10">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 10...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					<div class="gamer-item gamer-num-11">
						<div class="gamer-info"><p><a href="#" class="game-user">UserName 11...</a></p><span class="poker-bet money-after lite-green bold">659</span></div>
						<div class="progress-radial-container" style=""><div class="progress-radial progress-100"></div></div>
					</div>
					
					<?php include("game-table-text.php"); ?>
					
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="bottom-curve col-md-12">
			<img class="stretches" src="assets/img/bottom-curve.png">
		</div>
	</div>
	
	<?php include("live-games.php");?>