<!DOCTYPE html>
<html lang="ru">
  <head>
    <?php include("head.php"); ?>
  </head>

  <body>
	<div class="page-wrap-main-game">
		<?php // include("navbar.php");?>
		
		<?php include("main-game-top.php");?>

		<?php include("game-table-2.php"); ?>
	</div>
	<!--<footer class="site-footer">
		<?php // include("footer.php"); ?>
	</footer>-->
	
	<?php // include("chat.php"); ?>
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	
	<?php include("popup.php"); ?>
	
	
  </body>
</html>