<!-- Текст в центре демо стола -->
<div class="demo-table-text">
	<h1>7<span> \ </span>12</h1>
	<h2>Ожидание игроков</h2>
	<div class="line"></div>
	<p><span class="money-before valuta">700 - 4500</span> <b>\</b> обычная игра</p>
</div>

<!-- Текст в центре основного стола -->
<div class="main-table-text">
	<h3>Префлоп</h3>
	<div class="game-status-bar"><div class="red-line" style="width:60%"></div></div>
	<div class="bank-container clearfix">
		<div class="bank">
			<h1><span class="money-after">11557</span></h1>
			<div class="line-title-block">
				<div class="line-title">
					<span class="line-holder line-left"></span>
					<span>Общий банк</span>
					<span class="line-holder line-right"></span>
				</div>
			</div>
		</div>
		<div class="bank">
			<h1><span class="money-after">7557</span></h1>
			<div class="line-title-block">
				<div class="line-title">
					<span class="line-holder line-left"></span>
					<span>Общий банк</span>
					<span class="line-holder line-right"></span>
				</div>
			</div>
		</div>
		<div class="bank">
			<h1><span class="money-after">329</span></h1>
			<div class="line-title-block">
				<div class="line-title">
					<span class="line-holder line-left"></span>
					<span>Общий банк</span>
					<span class="line-holder line-right"></span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="personal-game-info">
	<p>Моя ставка</p>
	<div class="my-bet">
		<h1><span class="money-after">150</span></h1>
		<form>
			<input class="bet h1" type="text" name="bet" placeholder="0">
			<button type="reset" class="knob-l" name="" value="" title="Сброс"><span class="glyphicon glyphicon-remove"></span></button>
			<button type="submit" class="knob-r" name="" value="" title="Подтвердить"><span class="glyphicon glyphicon-ok"></span></button>
		</form>
		<div class="error-bet h1"><span>min.</span> 300</div>
	</div>
	<div class="button-container">
		<div class="control pass"><img src="assets/img/control-pass.png"></div>
		<div class="control compare"><img src="assets/img/control-compare.png"></div>
		<div class="control up"><img src="assets/img/control-up.png"></div>
		<div class="camera "><img src="assets/img/camera.png"></div><!-- Добавляется класс on, когда кнопка нажата и камера включена -->
	</div>
</div>