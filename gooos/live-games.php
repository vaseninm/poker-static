<!-- Список игр под столом -->
<div class="container relative separator-live-game">

	<div class="live-game-block clearfix">
		<div class="col-sm-4 col-md-3">
			<div class="game-block create-game">
				<img class="live-game-bg" src="assets/img/create-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<a href="#" class="create js-create"><span class="glyphicon glyphicon-plus"></span> Создать <b>игру</b></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="game-block goes-game">
				<img class="live-game-bg" src="assets/img/goes-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<h1 class="participants">7<span> \ </span>12</h1>
							<div class="lite-green">Сбор игроков</div>
							<a href="" class="goes">Присоединиться</a>
						</div>
					</div>
				</div>
				<div class="game-info-more">
					<div class="game-title">Техасский холдем</div>
					<div class="game-user-block">
						<p><a class="game-user" href="#">Droit</a></p>
						<p><a class="game-user" href="#">Vaseninm</a></p>
						<p><a class="game-user admin" href="#">Simb</a></p>
						<p><a class="game-user" href="#">Maaaria</a></p>
						<p><a class="game-user" href="#">Тот самый алькапоне</a></p>
						<p><a class="game-user" href="#">Max</a></p>
						<p><a class="game-user" href="#">Opertuner</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="game-block goes-game">
				<img class="live-game-bg" src="assets/img/goes-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<h1 class="participants">3<span> \ </span>7</h1>
							<div class="lite-green">Сбор игроков</div>
							<a href="" class="goes">Присоединиться</a>
						</div>
					</div>
				</div>
				<div class="game-info-more">
					<div class="game-title">Техасский холдем</div>
					<div class="game-user-block">
						<p><a class="game-user" href="#">Droit</a></p>
						<p><a class="game-user" href="#">Vaseninm</a></p>
						<p><a class="game-user admin" href="#">Simb</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="game-block goes-game">
				<img class="live-game-bg" src="assets/img/goes-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<h1 class="participants">12<span> \ </span>12</h1>
							<div class="lite-green">Сбор игроков</div>
							<a href="" class="goes">Присоединиться</a>
						</div>
					</div>
				</div>
				<div class="game-info-more">
					<div class="game-title">Техасский холдем</div>
					<div class="game-user-block">
						<p><a class="game-user" href="#">Droit</a></p>
						<p><a class="game-user" href="#">Vaseninm</a></p>
						<p><a class="game-user admin" href="#">Simb</a></p>
						<p><a class="game-user" href="#">Maaaria</a></p>
						<p><a class="game-user" href="#">Тот самый алькапоне</a></p>
						<p><a class="game-user" href="#">Max</a></p>
						<p><a class="game-user" href="#">Opertuner</a></p>
						<p><a class="game-user" href="#">Maaaria</a></p>
						<p><a class="game-user" href="#">Тот самый алькапоне</a></p>
						<p><a class="game-user" href="#">Max</a></p>
						<p><a class="game-user" href="#">Opertuner</a></p>
						<p><a class="game-user" href="#">Droit</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="game-block watch-game">
				<img class="live-game-bg" src="assets/img/watch-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<h1 class="participants">3<span> \ </span>3</h1>
							<div class="lite-brown">Игра идёт</div>
							<a href="" class="watch">Посмотреть</a>
						</div>
					</div>
				</div>
				<div class="game-info-more">
					<div class="game-title">Техасский холдем</div>
					<div class="game-user-block">
						<p><a class="game-user" href="#">Droit</a></p>
						<p><a class="game-user" href="#">Vaseninm</a></p>
						<p><a class="game-user admin" href="#">Simb</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="game-block watch-game">
				<img class="live-game-bg" src="assets/img/watch-game.png">
				<div class="game-info">
					<div class="table">
						<div class="cell">
							<h1 class="participants">12<span> \ </span>12</h1>
							<div class="lite-brown">Игра идёт</div>
							<a href="" class="watch">Посмотреть</a>
						</div>
					</div>
				</div>
				<div class="game-info-more">
					<div class="game-title">Техасский холдем</div>
					<div class="game-user-block">
						<p><a class="game-user" href="#">Droit</a></p>
						<p><a class="game-user" href="#">Vaseninm</a></p>
						<p><a class="game-user admin" href="#">Simb</a></p>
						<p><a class="game-user" href="#">Maaaria</a></p>
						<p><a class="game-user" href="#">Тот самый алькапоне</a></p>
						<p><a class="game-user" href="#">Max</a></p>
						<p><a class="game-user" href="#">Opertuner</a></p>
						<p><a class="game-user" href="#">Maaaria</a></p>
						<p><a class="game-user" href="#">Тот самый алькапоне</a></p>
						<p><a class="game-user" href="#">Max</a></p>
						<p><a class="game-user" href="#">Opertuner</a></p>
						<p><a class="game-user" href="#">Droit</a></p>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<?php include("create-game.php"); ?>
</div>
	