<!DOCTYPE html>
<html lang="ru">
  <head>
    <?php include("head.php"); ?>
  </head>

  <body>
	<div class="page-wrap">
		<?php include("navbar.php");?>

		<?php include("game-table.php"); ?>
	</div>
	<footer class="site-footer">
		<?php include("footer.php"); ?>
	</footer>
	
	<?php include("chat.php"); ?>
	

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	
	<?php include("popup.php"); ?>
	
	<!-- Создание игры, пригласить друзей, показать скрыть -->
	<script type="text/javascript">	
		$(function() {
			$(".js-create").click(function(){
				$(".create-game-container").css("display", "block");
				return false;
			});
			$(".js-close-game-container").click(function(){
				$(".create-game-container").css("display", "none");
				return false;
			});
			
			$(".js-invite-friends").click(function(){
				$(".invite-friends").css("display", "block");
				return false;
			});
			$(".js-close-friends").click(function(){
				$(".invite-friends").css("display", "none");
				return false;
			});
		});
	</script>
	
  </body>
</html>