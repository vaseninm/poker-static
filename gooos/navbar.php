<!-- Навигация -->
<div class="logo-top"></div>

<div class="top-nav-bg">
	<div class="top-nav-container">
		<div class="top-nav-block">
			<ul>
				<li><a href="#">Начало</a></li>
				<li><a href="#">Правила</a></li>
				<li class="separator"></li>
				<li><a href="#">О проекте</a></li>
				<li><a href="#">Контакты</a></li>
			</ul>
		</div>
	</div>	
</div>

<div class="arch text-center">
	<div class="user-info inlb text-right">
		<a class="user-profile" href="#">User name</a>
		<a class="user-exit" href="#"></a>		
	</div>
	<div class="separator inlb"></div>
	<div class="user-info inlb text-left">
		<span class="money-before valuta">8.754</span>
	</div>
</div>

