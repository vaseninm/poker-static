<!-- popup -->
	<div id="parent_popup">
		<div class="table">
			<div class="cell">
				<div class="windowContainer">
					<div class="borderT">
						<div class="borderR">
							<div class="borderL">
								<div class="borderB">
									<div class="popup-content">
									
										<!-- Содержимое поп-ап окна -->
										<div class="popup-title">
											<div class="cell">Приглашение</div>
										</div>
										<p>Введите код приглашения </p>
										<form>
											<input class="input" type="text" name="invite" placeholder="Код приглашения">
											<div class="buttons">
												<a href="#" class="button-brown">
													<div class="cell">Готово</div>
												</a>
											</div>
										</form>
										<!-- /Содержимое поп-ап окна -->
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="cornerTL"></div>
					<div class="cornerTR"></div>
					<div class="cornerBR"></div>
					<div class="cornerBL"></div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">	
		$(function() {
			$("#open-popap").click(function(){
				$("#parent_popup").show(200);
			});
		});
	</script>