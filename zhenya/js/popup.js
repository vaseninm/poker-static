$(function(){

    var $popup = $('#popup');
    var $overlay = $('#popup_overlay');

    positionCenter($popup); 

    $(".button").click(function(){ 
	$([$popup[0], $overlay[0]]).fadeIn(300); 
    }); 

    $($overlay).click(function(){ 
	$([$popup[0], $overlay[0]]).fadeOut(300); 
    }); 

    function positionCenter(elem){ 
	elem.css({ 
	    marginTop:'-'+elem.height() / 2 + 'px', 
	    marginLeft:'-'+elem.width() / 2 + 'px' 
	}); 
    }
});
